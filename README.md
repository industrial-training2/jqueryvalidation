# JQuery Form Validation

## Steps to use jquery form validation
* Download Jquery Validation and add them to your scripts from [Jquerty Validation Plugin](https://github.com/jquery-validation/jquery-validation/releases/tag/1.19.5) or add the below tags to your files
``` 
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script> 
``` 
* Download form-validation.js, style.css and include them into your assets.
* call validateForm function and pass jqeury form object as an argument. For Example
``` validateForm($("#myForm")) ```

## How to define your own rules and messages
Use addClassRules() to add custom rules and messages to desired fields.
* addClassRules() takes a classname to add rules to and rules object. For Example  
``` 
let rule = {
    minlength: 10,
    messages: {
        minlength: "Please enter a valid phone number"
    }
}
addClassRules("phone", rule); 
```