function initializeRules() {
    $(".required").rules("add", {
        required: true,
        messages: {
            required: "This field is required"
        }
    });
    
    $(".number").rules("add", {
        number: true,
        messages: {
            number: "Please enter a valid number"
        }
    });
    
    $(".email").rules("add", {
        email: true,
        messages: {
            email: "Please enter a valid email"
        }
    });

    $(".date").rules("add", {
        date: true,
        messages: {
            date: "Please enter a valid date",
        }
    })
    
    $(".url").rules("add", {
        url: true,
        messages: {
            url: "Please enter a valid url"
        }
    });
    
    $(".digits").rules("add", {
        number: true,
        messages: {
            required: "Please enter only valid digits"
        }
    });
    
}
function addClassRules(className, rule) {
    $("."+className).rules("add", rule);
}

function validateForm(form) {
    form.validate();
}